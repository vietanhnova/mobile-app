import React from 'react'
import { Provider } from 'react-redux'
import { Provider as PaperProvider } from 'react-native-paper'
import AppNavigator from './src/navigation'
import store from './src/common/store'

export default function App () {
  console.disableYellowBox = true
  return (
    <Provider store={store}>
      <PaperProvider>
        <AppNavigator />
      </PaperProvider>
    </Provider>
  )
}
