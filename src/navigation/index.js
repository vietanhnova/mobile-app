import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Login from '../screens/Login'
import Dashboard from '../screens/Dashboard'
import DeviceList from '../screens/DeviceList'
const StackNavigator = createStackNavigator(
  {
    Login: {
      screen: Login
    },
    DashBoard: {
      screen: Dashboard
    },
    DeviceList: {
      screen: DeviceList
    }
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
    mode: 'modal'
  }
)

export default createAppContainer(StackNavigator)
