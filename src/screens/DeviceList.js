import React from 'react'
import { ScrollView,View } from 'react-native'
import { connect } from 'react-redux'
import Header from '../components/Header'
import { List, IconButton, Snackbar } from 'react-native-paper'
import ControlDevice from '../components/ControlDevice'
import VoiceControl from '../components/VoiceControl'
import { controlDevice, lockDevice } from '../common/actions'
import BaseAPI from '../common/index'
import AddDevice from '../components/AddDevice'
class DeviceList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      user: {},
      isNotified: true,
      isLoading: true,
      curArea: this.props.curArea,
      devices: this.props.devices,
      openDialog: false,
      notify: {
        mess: '',
        status: true
      },
      deviceItem: {}
    }
  }
  componentDidMount () {
    this.getData()
  }

  componentDidUpdate () {
    console.log(this.props.curArea)
  }
  updateDevices = (devices, curArea) => {
    const deviceList = this.getDevicesByArea(devices, curArea)
    this.setState({ devices: deviceList })
  }
  getDevicesByArea = (devices, areaName) => {
    if (!areaName) {
      return devices
    }
    switch (areaName) {
    case 'Tất cả':
      return devices
    case 'Chia sẻ': {
      let shareDevices = devices.filter(device => device.createdUser !== this.state.user.email)
      return shareDevices
    }
    default: {
      let devicesList = devices.filter(device => device.createdUser === this.state.user.email && device.areaName === areaName)
      return devicesList
    }
    }
  }
  getData = async () => {
    console.log(this.props.curArea)
    const user = await BaseAPI.getItemStorage('user')
    if (user) {
      this.setState({ user: JSON.parse(user) })
    }
    const { devices, curArea } = this.props

    this.updateDevice(devices, curArea)
    this.setState({ isLoading: false })
  }

  onHandleDialog = (device) => () => {
    this.setState({ openDialog: !this.state.openDialog, deviceItem: device })
  }
  pushNotify = () => (
    this.state.notify?.mess === '' ? <></> : <Snackbar
      visible={this.state.isNotified}
      onDismiss={() => this.setState({ isNotified: false })}
      duration={500}
      action={{
        label: `${this.state.notify?.status ? 'Đóng' : ''}`,
        onPress: () => {
          // Do something
        }
      }}
    >
      {this.state.notify.mess}
    </Snackbar>
  )

  getDeviceIcon = (type, lastStatus) => {
    if (type === '1') {
      if (lastStatus) {
        return 'lightbulb-on'
      } else {
        return 'lightbulb-off'
      }
    }
    if (type === '2') {
      if (lastStatus) {
        return 'fan'
      } else {
        return 'fan-off'
      }
    }
    if (type === '3') {
      if (lastStatus) {
        return 'lightbulb-on-outline'
      } else {
        return 'lightbulb-off-outline'
      }
    }
  }
  updateDevice = (device) => () => {
    const { user } = this.state
    if (user.id !== device.createdUser && device.blockDevice) {
      const notify = {
        mess: `${device.createdUser} đã khoá ${device.name}`,
        status: true
      }
      this.setState({ notify: notify })
    } else {
      let status = !device.lastStatus
      const payload = {
        key: device.key,
        data: {
          type: 'controllDevice',
          deviceId: device.id,
          value: 0,
          status: status
        }
      }
      controlDevice(payload)
      const notify = {
        mess: `${device.name} đã ${device.lastStatus ? 'Bật' : 'Tắt'}`,
        status: true
      }
      this.setState({ notify: notify, isNotified: true })
    }
  }
  onLockDevice = (device) => async () => {
    const body = {
      deviceId: device.id,
      blockDevice: !device.blockDevice
    }
    const data = await BaseAPI.putUpdateData(body, '/device/block')
    if (data) {
      const payload = {
        type: 'updateBlockDevice',
        key: data.key,
        data: data
      }
      lockDevice(payload)
      const notify = {
        mess: `${device.name} đã ${device.blockDevice ? 'Khoá' : 'Mở khoá'}`,
        status: true
      }
      this.setState({ notify: notify, isNotified: true })
    }
  }
  render () {
    const { isLoading } = this.state
    const { curArea, devices } = this.props
    console.log(devices)
    return (
      <>
        <Header titleText={curArea || 'Thiết bị'} />
        {!isLoading && <ScrollView>
          {
            this.getDevicesByArea(devices, curArea).map((device, key) => (
              <View key={key}> 
              {device.powerOn?
                <List.Item
                style={device.lastStatus && { backgroundColor: 'rgb(255, 255, 0)' }}
                title={device.name}
                description={`${device.areaName === curArea ?'':`${device.areaName} | `}Trạng thái : ${device.lastStatus?'Bật':'Tắt'} ${device.lastValue!==0 ? device.lastValue:''}`}
                right={props => <IconButton onPress={this.onHandleDialog(device)} size={25} {...props} icon='settings' />}
                left={props => <IconButton onPress={this.onLockDevice(device)} {...props} icon={device.blockDevice ? 'lock' : 'lock-open'} />}
                onPress={this.updateDevice(device)}/>
              : <List.Item
              style={{backgroundColor: 'red'} }
              title={device.name}
              description='Thiết bị ngắt kết nối'
              right={props => <IconButton size={25} {...props} icon='settings' />}
              left={props => <IconButton {...props} icon='power' />}
            />
             }
              </View>
            ))
          }
        </ScrollView>}
        <VoiceControl />
        <AddDevice />
        {this.state.openDialog && (<ControlDevice device={this.state.deviceItem} />)}
        {this.pushNotify()}
      </>
    )
  }
}
const mapStateToProps = state => {
  return {
    devices: state.devices,
    curArea: state.curArea
  }
}
export default connect(mapStateToProps)(DeviceList)
