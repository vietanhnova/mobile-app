import React from 'react'
import { View, StyleSheet, Alert } from 'react-native'
import { Avatar, TextInput, Button, Dialog } from 'react-native-paper'
import Header from '../components/Header'
import BaseAPI from '../common/index'
export default class Account extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      user: {},
      visible: false,
      oldPw: '',
      newPw: '',
      newPwConfirm: ''
    }
  }

  componentDidMount () {
    this.getUserData()
  }
  getUserData = async () => {
    let user = await BaseAPI.getItemStorage('user')
    this.setState({ user: JSON.parse(user) })
  }
  updateUser=() => async () => {
    const { user } = this.state
    const res = BaseAPI.putUpdateData(user, '/user')
    if (res) {
      Alert.alert('Đã cập nhật thông tin')
    }
    this.getUserData()
  }
  changePw=() => async () => {
    const { oldPw, newPw, newPwConfirm, user } = this.state
    if (newPw !== newPwConfirm) {
      Alert.alert('Vui lòng kiểm tra lại')
    }
    const data = {
      id: user.id,
      oldPassword: oldPw,
      newPassword: newPw
    }
    const res = await BaseAPI.putUpdateData(data, '/pwChange')
    console.log(res)
    if (res) {
      Alert.alert('Đổi mật khẩu thành công')
      this.onHideModal()
    }
  }
  onShowModal = () => this.setState({ visible: true })
  onHideModal = () => this.setState({ visible: false })
  render () {
    console.log(this.state.user)
    const { user } = this.state
    return (
            <>
              <Header titleText='Tài khoản' />
              <View style={{ alignItems: 'center', marginTop: 20 }}>
                <Avatar.Text size={80} label='VN' />
                <TextInput
                  style={styles.textInput}
                  label='Họ tên '
                  mode='outlined'
                  value={user.fullName}
                  onChangeText={fullName => this.setState({ user: { ...user, fullName } })}
                />
                <TextInput
                  style={styles.textInput}
                  label='Email'
                  mode='outlined'
                  value={user.email}
                  disabled
                />
                <TextInput
                  style={styles.textInput}
                  label='Địa chỉ '
                  mode='outlined'
                  value={user.address}
                  onChangeText={address => this.setState({ user: { ...user, address } })}

                />
                <TextInput
                  style={styles.textInput}
                  label='Điện thoại '
                  mode='outlined'
                  value={user.phone}
                  onChangeText={phone => this.setState({ user: { ...user, phone } })}
                />
                <View style={{ flexDirection: 'row', marginTop: 15 }}>
                  <Button mode='contained' onPress={this.updateUser()}>Lưu</Button>
                  <Button style={{ marginLeft: 15 }}
                    mode='contained'
                    onPress={this.onShowModal}
                  >Đổi mật khẩu</Button>
                </View>
              </View>
              <Dialog
                style={styles.dialog}
                visible={this.state.visible}
                onDismiss={this.onHideModal}>
                <Dialog.Title>Đổi mật khẩu </Dialog.Title>
                <Dialog.Content>
                  <TextInput
                    style={styles.pwInput}
                    label='Mật khẩu cũ'
                    mode='outlined'
                    value={this.state.oldPw}
                    secureTextEntry
                    onChangeText={oldPw => this.setState({ oldPw })}
                  />
                  <TextInput
                    style={styles.pwInput}
                    label='Mật khẩu mới'
                    mode='outlined'
                    value={this.state.newPw}
                    secureTextEntry
                    onChangeText={newPw => this.setState({ newPw })}
                  />
                  <TextInput
                    style={styles.pwInput}
                    label='Nhập lại mật khẩu mới'
                    mode='outlined'
                    secureTextEntry
                    value={this.state.newPwConfirm}
                    onChangeText={newPwConfirm => this.setState({ newPwConfirm })}
                  />
                </Dialog.Content>
                <Dialog.Actions>
                  <Button onPress={this.changePw()}>Thay đổi</Button>
                </Dialog.Actions>
              </Dialog>
            </>
    )
  }
}
const styles = StyleSheet.create({
  textInput: {
    marginTop: 15,
    height: 30,
    width: 250
  },
  dialog: {
    position: 'absolute',
    left: 40,
    top: 100,
    width: 250
  },
  pwInput: {
    marginTop: 10,
    height: 30,
    width: 200
  }
})
