import React from 'react'
import { View, ScrollView, Dimensions, StyleSheet, Text } from 'react-native'
import Header from '../components/Header'
import { Avatar, Card, IconButton } from 'react-native-paper'
import { connect } from 'react-redux'
import AddArea from '../components/AddArea'
import AreaItem from '../components/AreaItem'
import VoiceControl from '../components/VoiceControl'
import BaseAPI from '../common/index'

const { width: WIDTH } = Dimensions.get('window')
const LeftContent = (props, name) => <Avatar.Icon {...props} icon={name} />
class AreaList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      user: {},
      devices: [],
      openDialog: false,
      deviceItem: {}
    }
  }
  componentDidMount () {
    this.getUser()
  }
  getUser = async () => {
    const user = await BaseAPI.getItemStorage('user')
    if (user) {
      this.setState({ user: JSON.parse(user) })
    }
  }
  getSharedDevices = () => {
    let devices = this.props.devices.filter(device => device.createdUser !== this.state.user.email)
    return devices
  }
  navigate = (areaName) => () => {
    this.props.setCurArea(areaName)
    this.props.jumpTo('deviceList')
  }
  getAreas = () => {
    const { areas } = this.props
    const body = []
    for (let i = 0; i < areas.length; i = i + 2) {
      body.push(
        (<View style={{ flexDirection: 'row' }} key={i}>
          <AreaItem areaName={areas[i]?.name} navigate={this.navigate} />
          {areas[i + 1] && <AreaItem areaName={areas[i + 1].name} navigate={this.navigate} />}
        </View>)
      )
    }
    return body
  }
  render () {
    const { sensor } = this.props
    return (
      <>
        <Header titleText='Nhà' />
        <View style={styles.container}>
          <Card style={styles.sensorCard}>
            <Card.Content style={{ flexDirection: 'row', justifyContent: 'space-around', top: -10 }}>
              <View>
                <Text>Nhiệt độ</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <Text style={{ fontSize: 16, top: 10, left: 10 }}>{sensor.temp}</Text>
                  <IconButton style={{ top: -5 }} icon='temperature-celsius' />
                </View>
              </View>
              <View>
                <Text>Độ ẩm</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                  <Text style={{ fontSize: 16, top: 10, left: 5 }}>{sensor.humi}</Text>
                  <IconButton style={{ top: -5, right: 7 }} icon='water-percent' />
                </View>
              </View>
            </Card.Content>
          </Card>
          <View style={{ flexDirection: 'row' }}>
            <Card style={styles.cardArea} onPress={this.navigate('Tất cả')}>
              <Card.Title title='Tất cả'
                style={styles.cardTitle}
                left={props => LeftContent(props, 'format-list-bulleted')}
                subtitle={`${this.props.devices.length} thiết bị`} />
              <Card.Actions>
              </Card.Actions>
            </Card>
            <Card style={styles.cardArea} onPress={this.navigate('Chia sẻ')}>
              <Card.Title title='Chia sẻ'
                style={styles.cardTitle}
                left={props => LeftContent(props, 'share-variant')}
                subtitle={`${this.getSharedDevices().length} thiết bị`} />
              <Card.Actions>
              </Card.Actions>
            </Card>
          </View>
          <ScrollView >
            {this.getAreas()}
          </ScrollView>
        </View>
        <VoiceControl />
        <AddArea />
      </>
    )
  }
}
const mapStateToProps = state => {
  return {
    areas: state.areas,
    devices: state.devices,
    sensor: state.sensor
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    setCurArea: (data) => {
      dispatch({
        type: 'SET_CURRENT_AREA',
        curArea: data
      })
    }
  }
}
const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'flex-start' },
  cardArea: { width: WIDTH / 2 - 10, height: 70, marginTop: 7, marginLeft: 7 },
  cardTitle: { justifyContent: 'flex-start', marginLeft: -10 },
  cardContent: { justifyContent: 'flex-start', marginLeft: -10 },
  listDevices: { flex: 1, height: 80, left: 0 },
  sensorCard: { width: WIDTH, height: 60 }
})
export default connect(mapStateToProps, mapDispatchToProps)(AreaList)
