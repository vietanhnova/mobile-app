import React from 'react'
import {
  StyleSheet, View, Text, Image, TextInput,
  ImageBackground, Dimensions, TouchableOpacity,
  findNodeHandle,
  Alert
} from 'react-native'
import { Button } from 'react-native-paper'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import bg from '../../assets/bg.jpeg'
import logo from '../../assets/logo.png'
import { Ionicons } from '@expo/vector-icons'
import BaseAPI from '../common/index'
import { connect } from 'react-redux'
import { activeSocket } from '../common/actions'
// import SocketService from '../common/socket'
const { width: WIDTH } = Dimensions.get('window')
// const { height: HEIGHT } = Dimensions.get('window')
class Login extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      hidePassword: true,
      username: 'lcnghia95@gmail.com',
      password: '123123123'
    }
  }
  showPass = () => () => {
    if (this.state.hidePassword) {
      this.setState({ hidePassword: false })
    } else {
      this.setState({ hidePassword: true })
    }
  }
  navigate = (page) => {
    this.props.navigation.navigate(page)
  }
  login = () => async () => {
    var body = {
      email: this.state.username,
      password: this.state.password,
      // email: 'anhnv@gmail.com',
      // email: 'lcnghia95@gmail.com',
      // password: '123123123',
      isLogin: true
    }
    let type = '/user/reg/pw'
    const payLoad = await BaseAPI.postCreateData(body, type)
    if (payLoad.errMess === 'namePwNotFound') {
      Alert.alert('Không thể đăng nhập')
    } else {
      BaseAPI.setItemStorage('auth', payLoad.jwtToken)
      BaseAPI.setItemStorage('user', JSON.stringify(payLoad.data))
      const devices = await BaseAPI.getData('/device')
      if (devices) {
        this.props.setDevices(devices.data)
      }
      const areas = await BaseAPI.getData('/area')
      // console.log(areas)
      if (areas) {
        this.props.setAreas(areas)
      }
      activeSocket()
      this.navigate('DashBoard')
    }
  }
  _scrollToInput (reactNode) {
    this.scroll.props.scrollToFocusedInput(reactNode)
  }
  resetPw=() => async () => {
    const { username } = this.state
    if (username !== '') {
      const body = {
        email: username
      }
      const res = await BaseAPI.postCreateData(body, '/user/pw/reset')
      if (res) {
        Alert.alert(res.message)
      }
    }
  }
  render () {
    return (
      <ImageBackground source={bg} style={styles.container}>
        <View style={styles.logoContainer}>
          <Image source={logo} style={styles.logo} />
        </View>
        <KeyboardAwareScrollView
          innerRef={ref => {
            this.scroll = ref
          }}
        >
          <View>
            <View style={styles.inputContainer}>
              <Ionicons name='ios-person' size={28} style={styles.inputIcon} />
              <TextInput
                style={styles.textInput}
                placeholder={'Tên đăng nhập'}
                placeholderTextColor={'rgba(255,255,255,0.7)'}
                underlineColorAndroid='transparent'
                onChangeText={(username) => this.setState({ username })}
              />
            </View>
            <View style={styles.inputContainer}>
              <Ionicons name='ios-lock' size={28} style={styles.inputIcon} />
              <TextInput
                style={styles.textInput}
                placeholder={'Mật khẩu'}
                placeholderTextColor={'rgba(255,255,255,0.7)'}
                underlineColorAndroid='transparent'
                secureTextEntry={this.state.hidePassword}
                onChangeText={(password) => this.setState({ password })}
                onFocus={(event) => {
                  this._scrollToInput(findNodeHandle(event.target))
                }}
              />
              <TouchableOpacity style={styles.eyeBtn}
                onPress={this.showPass()}>
                <Ionicons name={this.state.hidePassword ? 'ios-eye' : 'ios-eye-off'}
                  size={26} color={'rgba(255,255,255,0.7)'} />
              </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              <TouchableOpacity style={styles.loginBtn}
                onPress={this.login()}>
                <Text style={styles.loginText}>Đăng nhập</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'center', top: 5 }}>
            <Button onPress={this.resetPw()}>
              <Text style={{ color: 'white', fontSize: 10, textDecorationLine: 'underline' }}>Forgot Password ?</Text>
            </Button>
          </View>
        </KeyboardAwareScrollView>
      </ImageBackground>
    )
  }
}
const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center' },
  logoContainer: { marginTop: 20 },
  logo: { width: WIDTH, height: WIDTH },
  inputContainer: { marginTop: 10 },
  inputIcon: { position: 'absolute', top: 6, left: 38 },
  textInput: {
    width: WIDTH / 2,
    height: 40,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(0,0,0,0.35)',
    color: 'rgba(255,255,255,0.7)',
    marginHorizontal: 25
  },
  loginBtn: {
    width: WIDTH - 200,
    height: 40,
    borderRadius: 25,
    justifyContent: 'center',
    backgroundColor: '#003366',
    marginTop: 15
  },
  loginText: {
    color: 'rgba(255,225,255,0.7)', fontSize: 16, textAlign: 'center'
  },
  eyeBtn: { position: 'absolute', top: 8, right: 37 }
})
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    setDevices: (data) => {
      dispatch({
        type: 'SET_DEVICES',
        devices: data
      })
    },
    setAreas: (data) => {
      dispatch({
        type: 'SET_AREAS',
        areas: data
      })
    }
  }
}
const mapStateToProps = (state = {}) => {
  // console.dir(state)
  return { ...state }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login)
