import React from 'react'
import { BottomNavigation } from 'react-native-paper'
import AreaList from './AreaList'
import DeviceList from './DeviceList'
import Account from './Account'
class Dashboard extends React.Component {
  state = {
    index: 0,
    routes: [
      { key: 'areaList', title: 'Nhà của tôi', icon: 'home', color: '#3F51B5' },
      { key: 'deviceList', title: 'Thiết bị', icon: 'settings', color: '#009688' },
      { key: 'account', title: 'Tài khoản', icon: 'account', color: '#795548' }
    ]
  };
  _handleIndexChange = index => this.setState({ index });
  _renderScene = BottomNavigation.SceneMap({
    areaList: AreaList,
    deviceList: DeviceList,
    account: Account
  });
  render () {
    return (
      <BottomNavigation
        navigationState={this.state}
        // eslint-disable-next-line react/jsx-handler-names
        onIndexChange={this._handleIndexChange}
        renderScene={this._renderScene}
      />
    )
  }
}
export default Dashboard
