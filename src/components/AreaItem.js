import React from 'react'
import { ScrollView, Dimensions, StyleSheet,View } from 'react-native'
import { Avatar, List, Card, IconButton,Button } from 'react-native-paper'
import { connect } from 'react-redux'
import { controlDevice } from '../common/actions'
import BaseAPI from '../common/index'
const { width: WIDTH } = Dimensions.get('window')
const LeftContent = (props, name) => <Avatar.Icon {...props} icon={name}/>
class AreaItem extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      user: {},
      devices: [],
      deviceItem: {},
      isOpen: false
    }
  }
  componentDidMount () {
    this.getUser()
  }
  getTitleIcon = () => {
    switch (this.props.areaName) {
    case 'Phòng khách':
      return 'television-classic'
    case 'Phòng bếp':
      return 'silverware-fork-knife'
    case 'Phòng ngủ':
      return 'bed-empty'
    case 'Ban công':
      return 'table-border'
    default:
      return 'wall'
    }
  }
  getUser = async () => {
    const user = await BaseAPI.getItemStorage('user')
    if (user) {
      this.setState({ user: JSON.parse(user) })
    }
  }
  getDevicesByArea = () => {
    let devices = this.props.devices.filter(device => device.createdUser === this.state.user.email)
    return devices.filter(device => device.areaName === this.props.areaName)
  }
  getSharedDevices = () => {
    let devices = this.props.devices.filter(device => device.createdUser !== this.state.user.email)
    return devices
  }
  getDeviceIcon = (type, lastStatus) => {
    if (type === '1') {
      if (lastStatus) {
        return 'lightbulb-on'
      } else {
        return 'lightbulb-off'
      }
    }
    if (type === '2') {
      if (lastStatus) {
        return 'fan'
      } else {
        return 'fan-off'
      }
    }
    if (type === '3') {
      if (lastStatus) {
        return 'lightbulb-on-outline'
      } else {
        return 'lightbulb-off-outline'
      }
    }
  }
  // handleLockDevice = (device) => {

  // }
  updateDevice = (device) => () => {
    console.log('press update')
    let status = !device.lastStatus
    const payload = {
      key: device.key,
      data: {
        type: 'controllDevice',
        deviceId: device.id,
        value: 0,
        status: status
      }
    }
    controlDevice(payload)
  }
  render () {
    const { areaName } = this.props
    return (
      <>
        <Card style={styles.cardArea} onPress={this.props.navigate(areaName)}>
          <Card.Title title={this.props.areaName}
            style={styles.cardTitle}
            left={props => LeftContent(props, this.getTitleIcon())}
            subtitle={`${this.getDevicesByArea().length} thiết bị`} />
          <Card.Content style={styles.cardContent}>
            {this.getDevicesByArea().length > 0 ? <ScrollView style={styles.listDevices}>
              {
                this.getDevicesByArea().map((device, key) => (
                  <View key={key}> 
                    {device.powerOn
                    ?<List.Item
                    style={device.lastStatus && { backgroundColor: 'rgb(255, 255, 0)' }}
                    titleStyle={{ fontSize: 14, marginLeft: 0 }}
                    title={device.name}
                    left={props => <IconButton onPress={this.updateDevice(device)} size={20} {...props} icon={this.getDeviceIcon(device.signalTypeId, device.lastStatus)} />}
                    right={props => <IconButton {...props} size={20} icon={device.blockDevice ? 'lock' : 'lock-open'} />}
                  />
                  :<List.Item
                  style={{backgroundColor: 'red'} }
                  title={device.name}
                  titleStyle={{ fontSize: 14, marginLeft: 0 }}
                  right={props => <IconButton size={20} {...props} icon='alert-octagon-outline' />}
                  left={props => <IconButton {...props} size={20} icon='refresh' />}
                />}
                  </View>
                ))
              }
            </ScrollView> : (<></>)}
          </Card.Content>
          <Card.Actions>
          </Card.Actions>
        </Card>
      </>
    )
  }
}
const mapStateToProps = state => {
  return {
    areas: state.areas,
    devices: state.devices
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'flex-start' },
  cardArea: { width: WIDTH / 2 - 10, marginTop: 7, marginLeft: 7 },
  cardTitle: { justifyContent: 'flex-start', marginLeft: -10 },
  cardContent: { justifyContent: 'flex-start', marginLeft: -10 },
  listDevices: { flex: 1, height: 80, left: 0 }
})
export default connect(mapStateToProps)(AreaItem)
