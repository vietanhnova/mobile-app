import React from 'react'
import { StyleSheet, Dimensions, View } from 'react-native'
import { Dialog, Button, ToggleButton, Title, Snackbar } from 'react-native-paper'
import DeviceSetting from './DeviceSetting'
import EditDevice from './EditDevice'
import ShareDevice from './ShareDevice'
import DeviceHistory from './DeviceHistory'
import DeviceSchedule from './DeviceSchedule'
const { width: WIDTH } = Dimensions.get('window')
export default class ControlDevice extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: true,
      device: this.props.device,
      tabId: '1',
      deviceName: this.props.device?.name,
      password: '',
      userId: [],
      isNotified: true,
      notify: {
        mess: '',
        status: true
      }
    }
  }
  onHideModal = () => () => {
    this.setState({ visible: false })
    // this.props.onHandleDialog()
  }
  onSaveDevice = () => () => {
    this.onHideModal()
    // this.props.onHandleDialog()
  }
  pushNotify = () => (
    this.state.notify?.mess === '' ? <></> : <Snackbar
      visible={this.state.isNotified}
      onDismiss={() => this.setState({ isNotified: false })}
      duration={400}
      action={{
        label: `${this.state.notify?.status ? 'Đóng' : ''}`,
        onPress: () => {
          // Do something
        }
      }}
    >
      {this.state.notify.mess}
    </Snackbar>
  )
  handleNotify=(notify) => {
    this.setState({ notify: notify })
  }
  getDialogContent () {
    switch (this.state.tabId) {
    case '1':
      return <DeviceSetting device={this.state.device} />
    case '2':
      return <EditDevice device={this.state.device} />
    case '3':
      return <DeviceSchedule device={this.state.device} handleNotify={this.handleNotify} />
    case '4':
      return <ShareDevice device={this.state.device} handleNotify={this.handleNotify} />
    case '5':
      return <DeviceHistory device={this.state.device} />
    default:
      return <DeviceSetting device={this.state.device} />
    }
  }

  render () {
    const device = this.state.device
    return (
      <>
        <Dialog
          style={styles.dialog}
          visible={this.state.visible}
          onDismiss={this.onHideModal()}>
          {/* <Dialog.Title>{device?.name}</Dialog.Title> */}
          <Dialog.Content>
            <View style={{ flexDirection: 'row' }}>
              <Title>{device?.name}</Title>
              <ToggleButton.Row
                style={{ marginLeft: 20 }}
                onValueChange={tabId => this.setState({ tabId })}
                value={this.state.tabId} >
                <ToggleButton icon='settings' value='1' />
                <ToggleButton icon='content-save-edit' value='2' />
                <ToggleButton icon='calendar-clock' value='3' />
                <ToggleButton icon='share-variant' value='4' />
                <ToggleButton icon='history' value='5' />
              </ToggleButton.Row>
            </View>
            {this.getDialogContent()}
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={this.onSaveDevice()}>Đóng</Button>
          </Dialog.Actions>
        </Dialog>
        {this.pushNotify()}
      </>
    )
  }
}
const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0
  },
  dialog: {
    position: 'absolute',
    marginLeft: 0,
    top: 60,
    width: WIDTH
  },
  textInput: {
    marginTop: 10,
    width: 210,
    height: 40
  }
})
