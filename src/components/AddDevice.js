import React from 'react'
import { StyleSheet, Dimensions, View } from 'react-native'
import {
  FAB, Dialog, Button, TextInput, IconButton, Snackbar
} from 'react-native-paper'
import { Dropdown } from 'react-native-material-dropdown'
import BaseAPI from '../common/index'
import { connect } from 'react-redux'
import QRScanner from './QrScan'
import { typeOutputDevice } from '../common/constant'
const { width: WIDTH } = Dimensions.get('window')
const deviceTypes = [{ value: 'Quạt' }, { value: 'Đèn Dim' }]

class AddDevice extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      isNotified: true,
      types: [],
      deviceNames: [],
      areas: [],
      areaIds: [],
      notify: {
        mess: '',
        status: true
      },
      clipboardContent: null,
      openScanner: false,
      qrCode: 'ZFJRGc2Oe',
      device: {}
    }
  }

  onShowModal = () => this.setState({ visible: true });
  onHideModal = () => this.setState({ visible: false });

  handleAddDeviceNames = (name, index) => {
    const { deviceNames } = this.state
    deviceNames[index] = name
    this.setState({ deviceNames: deviceNames })
  }
  handleSelectArea = (area, index) => {
    const { areas } = this.state
    areas[index] = area
    this.setState({ areas: areas })
  }
  handleSelectType = (type, typeIndex, index) => {
    const { types } = this.state
    types[index] = typeIndex + 2
    this.setState({ types: types })
  }
  openScanner =() => () => {
    const { openScanner } = this.state
    this.setState({ openScanner: !openScanner })
  }
  dropdownList = () => {
    const { areas } = this.props
    let newArr = areas.map(area => ({ value: area.name }))
    return newArr
  }
  pushNotify = () => (
    this.state.notify?.mess === '' ? <></> : <Snackbar
      visible={this.state.isNotified}
      onDismiss={() => this.setState({ isNotified: false })}
      duration={10000000}
      action={{
        label: `${this.state.notify?.status ? 'Đóng' : ''}`,
        onPress: () => {
          // Do something
        }
      }}
    >
      {this.state.notify.mess}
    </Snackbar>
  )
  onAddDevices = () => async () => {
    const { types, areas, deviceNames, device } = this.state
    if (deviceNames.length !== types.length || areas.length !== types.length) {
      let notify = {
        mess: 'Không thể thêm mới',
        status: true
      }
      this.setState({ visible: false, notify: notify })
    }
    const lAreas = this.props.areas
    let data = []
    for (let i = 0; i < types.length; i++) {
      let index = lAreas.findIndex(item => item.name === areas[i])
      let areaId = lAreas[index].id
      data.push({
        name: deviceNames[i],
        areaName: areas[i],
        areaId: areaId,
        signalTypeId: types[i]
      })
    }
    const payload = {
      data: data,
      keyId: device.keyId,
      key: device.key,
      id: device.id
    }
    const res = await BaseAPI.postCreateData(payload, '/device/myDeivce')
    if (res) {
      const curDevices = await BaseAPI.getData('/device')
      if (curDevices) {
        this.props.setDevices(curDevices.data)
        let notify = {
          mess: 'Thêm thiết bị thành công',
          status: true
        }
        this.setState({ visible: false, notify: notify })
      }
    } else {
      let notify = {
        mess: 'Không thể thêm mới',
        status: true
      }
      this.setState({ visible: false, notify: notify })
    }

    console.log(payload)
  }
  findDevice = () => async () => {
    const { qrCode } = this.state
    if (qrCode !== '') {
      const type = '/myDevice/me/' + qrCode
      const device = await BaseAPI.getData(type)
      if (device) {
        if (device.errMess === 'deviceExistsUser') {
          let notify = {
            mess: 'Thiết bị đã có người dùng',
            status: true
          }
          this.setState({ notify: notify })
          return
        }
        const types = typeOutputDevice[device.type].value.map(item => item ? 1 : 0)
        this.setState({ types: types })
        this.setState({ device: device })
      }
    }
  }
  onChangeQrCode=(qrcode) => {
    this.setState({ qrCode: qrcode })
  }
  getDeviceByType=(type) => {
    if (type !== undefined) {
      const types = typeOutputDevice[type].value.map(item => item ? 1 : 0)
      this.setState({ types: types })
    }
  }

  render () {
    const { qrCode, types } = this.state
    return (
      <>
        <FAB
          style={styles.fab}
          icon='plus'
          onPress={this.onShowModal}
        />
        <Dialog
          style={styles.dialog}
          visible={this.state.visible}
          onDismiss={this.onHideModal}>
          <Dialog.Title>Thêm thiết bị </Dialog.Title>
          <Dialog.Content>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              <IconButton icon='qrcode-scan' onPress={this.openScanner()} />
              <TextInput
                mode='outlined'
                style={styles.textInput}
                label='Mã thiết bị'
                value={qrCode}
                onChangeText={qrCode => this.setState({ qrCode })}
              />
              <IconButton icon='file-find' onPress={this.findDevice()} />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              {this.state.openScanner && <QRScanner findDevice={this.findDevice()} onChangeQrCode={this.onChangeQrCode} />}
            </View>
            {types && types.map(
              (item, key) => (
                <View key={key} style={{ flexDirection: 'row', top: 10, left: -10 }}>
                  {item === 1 ? <TextInput
                    style={styles.deviceType}
                    mode='outlined'
                    label='Loại thiết bị'
                    value='đèn thường'
                  />
                    : <Dropdown
                      containerStyle={styles.deviceTypeList}
                      label='Loại thiết bị'
                      data={deviceTypes}
                      onChangeText={(typeId, typeIndex) => this.handleSelectType(typeId, typeIndex, key)}
                    />
                  }
                  <TextInput
                    style={styles.deviceType}
                    mode='outlined'
                    label='Tên thiết bị'
                    onChangeText={name => this.handleAddDeviceNames(name, key)}
                  />
                  <Dropdown
                    containerStyle={styles.areaList}
                    label='Khu vực'
                    data={this.dropdownList()}
                    onChangeText={area => this.handleSelectArea(area, key)}
                  />
                </View>
              ))}
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={this.onAddDevices()}>Thêm</Button>
          </Dialog.Actions>
        </Dialog>
        {this.pushNotify()}
      </>
    )
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    setDevices: (data) => {
      dispatch({
        type: 'SET_DEVICES',
        devices: data
      })
    }
  }
}
const mapStateToProps = (state = {}) => {
  return { ...state }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddDevice)
const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0
  },
  dialog: {
    position: 'absolute',
    top: 50,
    width: WIDTH,
    marginLeft: 0
  },
  deviceTypeList: {
    marginTop: -15,
    height: 30,
    width: WIDTH / 3 - 10
  },
  textInput: {
    height: 30,
    width: WIDTH / 2 - 10
  },
  areaList: {
    marginTop: -15,
    height: 30,
    width: WIDTH / 3
  },
  deviceType: {
    marginTop: 10,
    height: 30,
    width: WIDTH / 3 - 10
  }
})
