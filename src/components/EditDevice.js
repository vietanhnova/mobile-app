import React from 'react'
import { StyleSheet, View } from 'react-native'
import { TextInput, Button } from 'react-native-paper'
export default class EditDevice extends React.Component {
    state = {
      deviceName: this.props?.device.name,
      areaName: this.props?.device.areaName
    };
    render () {
      return (
        <>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <TextInput
              style={styles.textInput}
              label='Tên thiết bị'
              mode='outlined'
              value={this.state.deviceName}
              onChangeText={text => this.setState({ deviceName: text })}
            />
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <TextInput
              style={styles.textInput}
              label='Khu vực'
              mode='outlined'
              value={this.state.areaName}
              onChangeText={text => this.setState({ areaName: text })}
            />
          </View>
          <Button>Lưu</Button>
      </>
      )
    }
}
const styles = StyleSheet.create({
  textInput: {
    marginTop: 10,
    width: 210,
    height: 40
  }
})
