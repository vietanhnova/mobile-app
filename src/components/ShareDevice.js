import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { TextInput, Button, Chip, IconButton } from 'react-native-paper'
import BaseAPI from '../common'
export default class ShareDevice extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      shareUser: this.props?.device.userId,
      userShareId: '',
      notify: {
        mess: '',
        status: true
      },
      isNotified: true
    }
  }
    onShareUser=() => async () => {
      const { userShareId } = this.state
      const { device } = this.props
      const payLoad = {
        deviceId: device.id,
        userShareId: userShareId,
        type: true
      }
      const res = await BaseAPI.putUpdateData(payLoad, '/device/share')
      if (res) {
        let notify = {
          mess: res.message,
          status: true
        }
        this.props.handleNotify(notify)
      }
    }
    render () {
      return (
        <View>
          {
            this.state.shareUser.map((item, key) => (
              <View key={key} style={{ flexDirection: 'row', top: 10 }}>
                <Text>{item}</Text>
              </View>
            ))
          }
          <TextInput
            style={styles.textInput}
            label='Thêm người dùng'
            mode='outlined'
            value={this.state.newUser}
            onChangeText={text => this.setState({ userShareId: text })}
          />
          <Button onPress={this.onShareUser()}>Chia sẻ</Button>
        </View>
      )
    }
}
const styles = StyleSheet.create({
  textInput: {
    marginTop: 20,
    width: 210,
    height: 40
  },
  userChip: {
    width: 100
  }
})
