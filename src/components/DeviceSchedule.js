import React, { Component } from 'react'
import { StyleSheet, View, Text, Dimensions } from 'react-native'
import TimePicker from 'react-native-simple-time-picker'
import { IconButton, TextInput, Checkbox, RadioButton, Switch } from 'react-native-paper'
import BaseAPI from '../common'
import { defaultCronPayload } from '../common/constant'
const { width: WIDTH } = Dimensions.get('window')

export default class DeviceSchedule extends Component {
  constructor (props) {
    super(props)
    this.state = {
      schedule: [],
      selectedHours: 0,
      selectedMinutes: 0,
      openTimePicker: false,
      radioBtnValue: 'oneTime',
      scheduleStatus: 'on',
      loopDay: [
        { 'CN': false },
        { 'thứ 2': false },
        { 'thứ 3': false },
        { 'thứ 4': false },
        { 'thứ 5': false },
        { 'thứ 6': false },
        { 'thứ 7': false }
      ]
    }
  }
  componentDidMount () {
    this.getSchedule()
  }
  getSchedule= async () => {
    const data = await BaseAPI.getData('/time')
    this.setState({ schedule: data })
  }
  handleSelectDate = (index) => () => {
    const loop = this.state.loopDay
    const key = Object.keys(loop[index])[0]
    const status = loop[index][key]
    loop[index][key] = !status
    // console.log(loop)
    this.setState({ loopDay: loop })
  }
  onAddSchedule=() => async () => {
    const { loopDay, selectedMinutes, selectedHours, radioBtnValue, scheduleStatus } = this.state
    const { device } = this.props
    const defaultDay = loopDay.map((day, key) =>
      day[Object.keys(day)[0]] ? key : ''
    )
    const loop = defaultDay.filter(item => item !== '')
    const timeExpression = [
      [selectedMinutes],
      [selectedHours],
      defaultCronPayload.defaultDate,
      defaultCronPayload.defaultMonth,
      loop.length !== 0 ? loop : defaultCronPayload.defaultDay
    ]
    const payload = {
      type: radioBtnValue,
      timeExpression,
      action: {
        key: device.key,
        data: {
          type: 'controllDevice',
          deviceId: device.id,
          value: 0,
          status: scheduleStatus === 'on'
        }
      }
    }
    const res = await BaseAPI.postCreateData(payload, '/time')
    if (res) {
      let notify = {
        mess: 'Đã thêm lịch',
        status: true
      }
      this.props.handleNotify(notify)
      this.getSchedule()
    }
  }
  onToggleSwitch=(item) => () => {
    const data = {
      id: item.id
    }
    const type = item.status === 'ACTIVE' ? '/time/cancel' : '/time/active'
    const res = BaseAPI.postCreateData(data, type)
    if (res) {
      let notify = {
        mess: `Đã ${item.status === 'ACTIVE' ? 'bật' : 'tắt'} hẹn giờ`,
        status: true
      }
      this.props.handleNotify(notify)
      this.getSchedule()
    }
  }
  onDelete=(item) => async () => {
    const payload = {
      id: item.id
    }
    const res = await BaseAPI.deleteData(payload, '/time')
    if (res) {
      let notify = {
        mess: 'Đã xoá lịch',
        status: true
      }
      this.props.handleNotify(notify)
      this.getSchedule()
    }
  }
  render () {
    const { selectedHours, selectedMinutes, openTimePicker, radioBtnValue, loopDay } = this.state
    const { schedule } = this.state
    // console.log(schedule)
    return (
      <View style={styles.container}>
        <View>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
            <Text style={{ top: 12 }}>Thao tác : </Text>
            <RadioButton.Group
              onValueChange={scheduleStatus => this.setState({ scheduleStatus })}
              value={this.state.scheduleStatus}
            >
              <View style={styles.statusBtn}>
                <RadioButton value='on' />
                <Text style={{ top: 5 }}>Bật</Text>
              </View>
              <View style={styles.statusBtn}>
                <RadioButton value='off' />
                <Text style={{ top: 5 }}>Tắt</Text>
              </View>
            </RadioButton.Group>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', top: 5 }}>
            <TextInput
              style={styles.textInput}
              mode='outlined'
              value={selectedHours.toString()}
              onChangeText={selectedHours => this.setState({ selectedHours })}
            />
            <Text style={{ top: 10 }}>:</Text>
            <TextInput
              style={styles.textInput}
              mode='outlined'
              value={selectedMinutes.toString()}
              onChangeText={selectedMinutes => this.setState({ selectedMinutes })}
            />
            <IconButton icon='clock-outline' onPress={() => this.setState({ openTimePicker: !openTimePicker })} />
            <RadioButton.Group
              onValueChange={radioBtnValue => this.setState({ radioBtnValue })}
              value={this.state.radioBtnValue}
            >
              <View style={styles.radioBtn}>
                <RadioButton value='oneTime' />
                <Text style={{ top: 5 }}>Một lần</Text>
              </View>
              <View style={styles.radioBtn}>
                <RadioButton value='loop' />
                <Text style={{ top: 5 }}>Lặp lại</Text>
              </View>
            </RadioButton.Group>
          </View>
          {openTimePicker && <TimePicker
            selectedHours={selectedHours}
            selectedMinutes={selectedMinutes}
            onChange={(hours, minutes) => this.setState({
              selectedHours: hours, selectedMinutes: minutes
            })}
          />}
          {radioBtnValue === 'loop' &&
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            {loopDay.map((day, key) => (
              <View key={key} style={styles.loopDate}>
                <Checkbox status={day[Object.keys(day)[0]] ? 'checked' : 'unchecked'}
                  onPress={this.handleSelectDate(key)}
                />
                <Text>{Object.keys(day)[0]}</Text>
              </View>
            ))}

          </View>
          }
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <IconButton icon='plus-circle' onPress={this.onAddSchedule()} />
          </View>
        </View>
        {
          schedule && schedule.map((item, key) =>
            <View key={key} style={styles.scheduleItem} >
              <IconButton style={{ bottom: 3 }} icon='delete' onPress={this.onDelete(item)} />
              <IconButton style={{ bottom: 3 }} icon={item.action.data.status ? 'lightbulb-on' : 'lightbulb-off'} />
              <View>
                <Text fontSize={styles.scheduleText}>
                  {`${item.timeExpression[1][0]} :${item.timeExpression[0][0]} `}
                </Text>
                <Text>{`${item.type === 'oneTime' ? 'Một lần' : 'Lặp'}`}</Text>
              </View>
              {item.type === 'loop' &&
              <View style={{ flexDirection: 'row' }}>
                {item.timeExpression[4].map((day, key) =>
                  <Text key={key}>{Object.keys(loopDay[day])[0]}</Text>
                )}
              </View>
              }
              <Switch value={item.status === 'ACTIVE'}
                style={styles.switchBtn}
                onValueChange={this.onToggleSwitch(item)} />
            </View>
          )}
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderStyle: 'dashed',
    borderColor: 'black',
    borderWidth: 0.5,
    borderRadius: 10,
    width: WIDTH - 20,
    right: 15,
    top: 10
  },
  textInput: {
    height: 30,
    width: 50
  },
  radioBtn: {
    flexDirection: 'row',
    top: 6,
    paddingRight: 6,
    borderStyle: 'solid',
    borderColor: 'black',
    borderWidth: 0.5,
    borderRadius: 10,
    height: 30
  },
  loopDate: {
    borderStyle: 'dashed',
    borderRadius: 5,
    borderWidth: 0.5,
    marginLeft: 10
  },
  scheduleItem: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  scheduleText: {
    fontSize: 20,
    top: 10,
    right: 10
  },
  statusBtn: {
    flexDirection: 'row',
    left: 5,
    top: 6,
    paddingRight: 6,
    height: 30
  },
  switchBtn: {
    left: 10
  }
})
