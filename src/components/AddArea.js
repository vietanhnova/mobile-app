import React from 'react'
import { StyleSheet } from 'react-native'
import { FAB, Dialog, Button, TextInput, Snackbar } from 'react-native-paper'
import BaseAPI from '../common/index'
import { connect } from 'react-redux'
class AddArea extends React.Component {
  state = {
    visible: false,
    isNotified: true,
    areaIndex: '1',
    areaName: '',
    password: '',
    userId: [],
    notify: {
      mess: '',
      status: true
    }
  };

  onShowModal = () => this.setState({ visible: true });
  onHideModal = () => this.setState({ visible: false });
  onAddArea = async () => {
    const { areaName } = this.state
    if (areaName !== '') {
      var body = {
        name: areaName
      }
      let type = '/area'
      const payLoad = await BaseAPI.postCreateData(body, type)
      if (payLoad) {
        const areas = await BaseAPI.getData('/area')
        if (areas) {
          this.props.setAreas(areas)
          const notify = {
            mess: `Đã thêm ${areaName} vào khu vực`,
            status: true
          }
          this.setState({ notify: notify, visible: false })
        }
        this.onHideModal()
      }
    }
  }
  pushNotify = () => (
    this.state.notify?.mess === '' ? <></> : <Snackbar
      visible={this.state.isNotified}
      onDismiss={() => this.setState({ isNotified: false })}
      duration={10000000}
      action={{
        label: `${this.state.notify?.status ? 'Đóng' : ''}`,
        onPress: () => {
          // Do something
        }
      }}
    >
      {this.state.notify.mess}
    </Snackbar>
  )
  render () {
    return (
      <>
        <FAB
          style={styles.fab}
          icon='plus'
          onPress={this.onShowModal}
        />
        <Dialog
          style={styles.dialog}
          visible={this.state.visible}
          onDismiss={this.onHideModal}>
          <Dialog.Title>Thêm khu vực </Dialog.Title>
          <Dialog.Content>
            <TextInput
              style={styles.textInput}
              label='Tên khu vực'
              mode='outlined'
              value={this.state.text}
              onChangeText={text => this.setState({ areaName: text })}
            />
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={this.onAddArea}>Thêm</Button>
          </Dialog.Actions>
        </Dialog>
        {this.pushNotify()}
      </>
    )
  }
}
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    setAreas: (data) => {
      dispatch({
        type: 'SET_AREAS',
        areas: data
      })
    }
  }
}
const mapStateToProps = (state = {}) => {
  return { ...state }
}
export default connect(mapStateToProps, mapDispatchToProps)(AddArea)
const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0
  },
  dialog: {
    position: 'absolute',
    left: 40,
    top: 150,
    width: 250
  },
  textInput: {
    marginTop: 10,
    width: 210,
    height: 40
  }
})
