import React from 'react'
import { View } from 'react-native'
import { DataTable } from 'react-native-paper'
import BaseAPI from '../common'
// import BaseAPI from '../common/index'
export default class DeviceHistory extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      history: [],
      page: 1
    }
  }
  componentDidMount () {
    this.getHistory()
  }
  getHistory = async () => {
    const history = await BaseAPI.getData('/history')
    if (history) {
      const deviceHistory = history.filter(item => item.deviceName === this.props.device.name)
      this.setState({ history: deviceHistory.slice(0, 5) })
    }
  }
  getBody = () => {
    return this.state.history.map((item, key) => (
      <DataTable.Row key={key}>
        <DataTable.Cell>{item.updatedAt}</DataTable.Cell>
        <DataTable.Cell >{item.status ? 'Bật' : 'Tắt'}</DataTable.Cell>
        <DataTable.Cell >{item.value}</DataTable.Cell>
        <DataTable.Cell >{item.createdUser}</DataTable.Cell>
      </DataTable.Row>
    ))
  }
  render () {
    // const { device } = this.props
    console.log(this.state.history)
    return (
      <>
        <View>
          <DataTable >
            <DataTable.Header>
              <DataTable.Title>Thời gian</DataTable.Title>
              <DataTable.Title>Thao tác</DataTable.Title>
              <DataTable.Title>Giá trị</DataTable.Title>
              <DataTable.Title>Người dùng</DataTable.Title>
            </DataTable.Header>
          </DataTable>
          {this.getBody()}
          <DataTable.Pagination
            page={this.state.page}
            numberOfPages={3}
            onPageChange={(page) => { this.setState({ page: page }) }}
            label='1-5 of 1'
          />
        </View>
      </>
    )
  }
}
