import React from 'react'
import { StyleSheet, View } from 'react-native'
import { FAB, Dialog, Button, IconButton, Text } from 'react-native-paper'
// import { Asset } from 'expo-asset'
import { Audio } from 'expo-av'
import * as FileSystem from 'expo-file-system'
// import * as Font from 'expo-font'
import * as Permissions from 'expo-permissions'
import axios from 'axios'
import BaseAPI from '../common'

export default class VoiceControl extends React.Component {
  constructor (props) {
    super(props)
    this.recording = null
    this.sound = null
    this.isSeeking = false
    this.shouldPlayAtEndOfSeek = false
    this.state = {
      visible: false,
      haveRecordingPermissions: false,
      isLoading: false,
      isPlaybackAllowed: false,
      muted: false,
      soundPosition: null,
      soundDuration: null,
      recordingDuration: null,
      shouldPlay: false,
      isPlaying: false,
      isRecording: false,
      fontLoaded: false,
      shouldCorrectPitch: true,
      volume: 1.0,
      rate: 1.0
    }
    this.recordingSettings = JSON.parse(JSON.stringify(Audio.RECORDING_OPTIONS_PRESET_LOW_QUALITY))
    this.recordingSettings.ios.extension = '.wav'
    this.recordingSettings.android.extension = '.wav'
    // console.log('setting', this.recordingSettings)
  }
  componentDidMount () {
    this._askForPermissions()
  }
  _askForPermissions = async () => {
    const response = await Permissions.askAsync(Permissions.AUDIO_RECORDING)
    this.setState({
      haveRecordingPermissions: response.status === 'granted'
    })
  }
  _updateScreenForRecordingStatus = status => {
    if (status.canRecord) {
      this.setState({
        isRecording: status.isRecording,
        recordingDuration: status.durationMillis
      })
    } else if (status.isDoneRecording) {
      this.setState({
        isRecording: false,
        recordingDuration: status.durationMillis
      })
      if (!this.state.isLoading) {
        this._stopRecordingAndEnablePlayback()
      }
    }
  };
  _uploadFile = async (uri) => {
    const token = await BaseAPI.getItemStorage('auth')
    let filename = uri.split('/').pop()
    let match = /\.(\w+)$/.exec(filename)
    let type = match ? `${match[1]}` : `audio`
    // eslint-disable-next-line no-undef
    let formData = new FormData()
    formData.append('file', { uri: uri, name: token, type })
    const config = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer ' + token
      }
    }
    const response = await axios.post('http://54.169.226.64:5000', formData, config)
    if (response.status === 200) {
      console.log(response)
    } else {
      return null
    }
  }
  async _stopPlaybackAndBeginRecording () {
    this.setState({
      isLoading: true
    })
    if (this.sound !== null) {
      await this.sound.unloadAsync()
      this.sound.setOnPlaybackStatusUpdate(null)
      this.sound = null
    }
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: true,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false,
      staysActiveInBackground: true
    })
    if (this.recording !== null) {
      this.recording.setOnRecordingStatusUpdate(null)
      this.recording = null
    }

    const recording = new Audio.Recording()
    await recording.prepareToRecordAsync(this.recordingSettings)
    recording.setOnRecordingStatusUpdate(this._updateScreenForRecordingStatus)

    this.recording = recording
    await this.recording.startAsync() // Will call this._updateScreenForRecordingStatus to update the screen.
    this.setState({
      isLoading: false
    })
  }
  async _stopRecordingAndEnablePlayback () {
    this.setState({
      isLoading: true
    })
    try {
      await this.recording.stopAndUnloadAsync()
    } catch (error) {
      // Do nothing -- we are already unloaded.
    }
    const info = await FileSystem.getInfoAsync(this.recording.getURI())
    // console.log(`FILE INFO: ${JSON.stringify(info)}`)
    if (info) {
      console.log('uri', info.uri)
      const response = await this._uploadFile(info.uri)
      console.log('voice', response)
    }
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      playsInSilentModeIOS: true,
      playsInSilentLockedModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      playThroughEarpieceAndroid: false,
      staysActiveInBackground: true
    })
    const { sound } = await this.recording.createNewLoadedSoundAsync(
      {
        isLooping: true,
        isMuted: this.state.muted,
        volume: this.state.volume,
        rate: this.state.rate,
        shouldCorrectPitch: this.state.shouldCorrectPitch
      },
      this._updateScreenForSoundStatus
    )
    this.sound = sound
    this.setState({
      isLoading: false
    })
  }
  onShowModal = () => this.setState({ visible: true });
  onHideModal = () => this.setState({ visible: false });
  _onRecordPressed = () => () => {
    this.setState({ isRecording: !this.state.isRecording })
    if (this.state.isRecording) {
      this._stopRecordingAndEnablePlayback()
    } else {
      this._stopPlaybackAndBeginRecording()
    }
  };
  render () {
    if (!this.state.haveRecordingPermissions) {
      return (
        <View style={styles.container}>
          <View />
          <Text style={{ textAlign: 'center' }}>
            You must enable audio recording permissions in order to use this app.
          </Text>
          <View />
        </View>
      )
    }
    const isRecording = this.state.isRecording
    return (
      <>
        <FAB
          style={styles.fab}
          icon='microphone-settings'
          onPress={this.onShowModal}
        />
        <Dialog
          style={styles.dialog}
          visible={this.state.visible}
          onDismiss={this.onHideModal}>
          <Dialog.Title>Xin chào! Hãy nói để điều khiển thiết bị</Dialog.Title>
          <Dialog.Content style={styles.content}>
            <IconButton icon={isRecording ? 'microphone-off' : 'microphone'}
              size={50} color='red' style={styles.VoiceControl}
              onPress={this._onRecordPressed()} />
            <Text>{isRecording ? 'Đang nghe' : 'Nhấn để nói'}</Text>
            {/* <IconButton icon='dots-horizontal' /> */}
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={this.onHideModal}>Đóng</Button>
          </Dialog.Actions>
        </Dialog>
      </>
    )
  }
}
const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 70
  },
  dialog: {
    position: 'absolute',
    left: 40,
    top: 200,
    width: 250
  },
  content: {
    alignItems: 'center'
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'stretch'
    // backgroundColor: BACKGROUND_COLOR,
    // minHeight: DEVICE_HEIGHT,
    // maxHeight: DEVICE_HEIGHT,
  }
})
