import React from 'react'
import { View, Text } from 'react-native'
import { IconButton } from 'react-native-paper'
import { controlDevice } from '../common/actions'

export default class DeviceSetiing extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      device: this.props.device,
      user: this.props.user
    }
  }
  getDeviceIcon = (type) => {
    if (type === '1') {
      return 'lightbulb-on'
    }
    if (type === '2') {
      return 'fan'
    }
    if (type === '3') {
      return 'lightbulb-on-outline'
    }
  }
  updateDevice = (device, type) => () => {
    let value = device.lastValue
    if (type === 'plus') {
      value = value + 1
    } else {
      value = value - 1
    }
    if (value < 0 || value > 3) {
      return
    }
    let status = device.lastStatus
    const payload = {
      key: device.key,
      data: {
        type: 'controllDevice',
        deviceId: device.id,
        value: value,
        status: status
      }
    }
    controlDevice(payload)
  }
  render () {
    const { device, user } = this.state
    console.log(user)
    return (
      <>
        <View>
          {device.signalTypeId === '1'
            ? <View style={{ flexDirection: 'row', top: 20, justifyContent: 'center' }}>
              {
                device.lastStatus || <Text>Thiết bị này đã tắt</Text>
              }
            </View>
            : <View>
              {
                device.lastStatus
                  ? <View>
                    <View style={{ flexDirection: 'row', top: 10, justifyContent: 'center' }}>
                      <Text>Điều chỉnh giá trị</Text>
                    </View>
                    <View style={{ flexDirection: 'row', top: 10, justifyContent: 'center' }}>
                      <IconButton icon='minus' onPress={this.updateDevice(device, 'minus')} />
                      <IconButton icon={this.getDeviceIcon(device.signalTypeId)} />
                      <IconButton icon='plus' onPress={this.updateDevice(device, 'plus')} />
                    </View>
                    <View style={{ flexDirection: 'row', top: 20, justifyContent: 'center' }}>
                      <Text>{device.lastValue}</Text>
                    </View>
                  </View>
                  : <View style={{ flexDirection: 'row', top: 20, justifyContent: 'center' }}>
                    <Text>Thiết bị này đã tắt</Text>
                  </View>
              }
            </View>
          }
        </View>
      </>
    )
  }
}
