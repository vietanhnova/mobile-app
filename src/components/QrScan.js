import React, { useState, useEffect } from 'react'
import { Text, View, Button } from 'react-native'
import { BarCodeScanner } from 'expo-barcode-scanner'

export default function QRScanner (props) {
  const [hasPermission, setHasPermission] = useState(null)
  const [scanned, setScanned] = useState(false)
  const { onChangeQrCode } = props

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync()
      setHasPermission(status === 'granted')
    })()
  }, [])

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true)
    console.log(type, data)
    onChangeQrCode(data.toString())
  }

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>
  }
  return (
    <View>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center'
        }}>
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          style={{ width: 200, height: 200 }}
        />

      </View>
      {scanned && (
        <Button title={'Nhấn để scan lại'} onPress={() => setScanned(false)} />
      )}
    </View>
  )
}
