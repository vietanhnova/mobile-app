import BaseAPI from '../common/index'
const initialState = { curArea: '', areas: [], sensor: { humi: 70, temp: 25 }, devices: BaseAPI.getItemStorage('devices') }
const reducer = (state = initialState, action) => {
  switch (action.type) {
  case 'SET_AREAS':
    BaseAPI.setItemStorage('areas', JSON.stringify(action.areas))
    return { ...state, areas: action.areas }
  case 'SET_DEVICES':
    BaseAPI.setItemStorage('devices', JSON.stringify(action.devices))
    return { ...state, devices: action.devices }
  case 'UPDATE_DEVICE': {
    // console.log('reducer -> action', action.devices[0])
    BaseAPI.setItemStorage('devices', JSON.stringify(action.devices))
    return { ...state, devices: [...action.devices] }
  }
  case 'UPDATE_SENSOR':
    return { ...state, sensor: action.sensor }
  case 'SET_CURRENT_AREA':
    return { ...state, curArea: action.curArea }
  }
}
export default reducer
