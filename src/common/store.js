import { createStore, applyMiddleware, compose } from 'redux'
import socketReducer from './reducer'
import thunkMiddleWare from 'redux-thunk'
const middleWare = [thunkMiddleWare]
const store = createStore(socketReducer, compose(applyMiddleware(...middleWare)))
export default store
