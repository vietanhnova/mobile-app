export const defaultCronPayload = {
  defaultMinute: [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
    12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
    24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
    36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,
    48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59
  ],
  defaultHour: [
    0, 1, 2, 3, 4, 5, 6, 7,
    8, 9, 10, 11, 12, 13, 14, 15,
    16, 17, 18, 19, 20, 21, 22, 23
  ],
  defaultDate: [
    1, 2, 3, 4, 5, 6, 7, 8, 9,
    10, 11, 12, 13, 14, 15, 16, 17, 18,
    19, 20, 21, 22, 23, 24, 25, 26, 27,
    28, 29, 30, 31
  ],
  defaultMonth: [
    1, 2, 3, 4, 5,
    6, 7, 8, 9, 10,
    11, 12
  ],
  defaultDay: [
    0, 1, 2, 3,
    4, 5, 6
  ]
}
export const typeOutputDevice = {
  // dim false light true
  T1: {
    name: '1 relay',
    value: [true]
  },
  T2: {
    name: '1 dimmer',
    value: [false]
  },
  T3: {
    name: '2 relay',
    value: [true, true]
  },
  T4: {
    name: '2 dimmer',
    value: [false, false]
  },
  T5: {
    name: '3 relay',
    value: [true, true, true]
  },
  T6: {
    name: '2 relay 1 dimmer',
    value: [true, true, false]
  },
  T7: {
    name: '3 dimmer',
    value: [false, false, false]
  }
}

export const typeDeviceAddNew = {
  // D1: {
  //   name: 'Đèn',
  //   value: 1
  // },
  D2: {
    name: 'Quạt',
    value: 2
  },
  D3: {
    name: 'Đèn Tăng Giảm',
    value: 3
  }
}
