import SocketIOClient from 'socket.io-client'
import { REACT_APP_SOCKET } from 'react-native-dotenv'
import BaseAPI from './index'
import store from './store'
import { get } from 'lodash'
let socketService
export const getToken = async () => {
  const token = await BaseAPI.getItemStorage('auth')
  return token
}
export const getDevices = (data) => ({
  type: 'GET_DEVICES',
  devices: data
})
export const updateDevices = (data) => ({
  type: 'UPDATE_DEVICES',
  devices: data
})
export const controlDevice = (data) => {
  socketService.emit('message', data)
  getMessageSocket(data)
}
export const lockDevice = (data) => {
  socketService.emit('message', data)
  getMessageSocket(data)
}
export const getMessageSocket = (payload) => {
  if (payload && payload.key) {
    let devices = store.getState().devices
    if (get(payload, 'type') === 'updateBlockDevice') {
      let findIndex = devices && devices.findIndex(item => get(payload, 'data.id') === item.id)
      if (findIndex !== -1) {
        devices[findIndex].blockDevice = payload.data.blockDevice
        store.dispatch({
          type: 'UPDATE_DEVICE',
          devices: devices
        })
      }
    } else {
      let findIndex = devices && devices.findIndex(item => get(payload, 'data.deviceId') === item.id)
      if (findIndex !== -1) {
        devices[findIndex].lastStatus = payload.data.status
        devices[findIndex].lastValue = payload.data.value
        store.dispatch({
          type: 'UPDATE_DEVICE',
          devices: devices
        })
      }
    }
  }
}
export const setDevices = (data) => {
  return (dispatch) => {
    dispatch(setDevices(data))
  }
}
export const activeSocket = (isReconnect = true) => {
  try {
    if (socketService) {
      socketService.removeAllListeners()
      socketService.disconnect()
      socketService = null
    }
    setTimeout(async () => {
      const _token = await getToken()
      console.log('active socket', _token)
      if (isReconnect && _token) {
        socketService = SocketIOClient(`${REACT_APP_SOCKET}?token=${_token}`, {
          // timeout: 10000,
          // jsonp: false,
          transports: ['websocket']
          // autoConnect: false,
          // agent: '-',
          // path: '/',
          // pfx: '-',
          // cert: '-',
          // ca: '-',
          // ciphers: '-',
          // rejectUnauthorized: '-',
          // perMessageDeflate: '-'
        })
        socketService.on('connect', async () => {
          console.log('connected socket ')
        })
        socketService.on('fmx_socketList', (event) => {
        })

        socketService.on('disconnect', (e) => {
          // logDebug('Disconnect socket')
          console.log('socket disconnected')
          activeSocket(true)
        })
        socketService.on('message', event => {
          if (event.payload.data.type === 'sensorUpdate') {
            const { humi, temp } = event.payload.data
            store.dispatch({
              type: 'UPDATE_SENSOR',
              sensor: { humi, temp }
            })
          }
          getMessageSocket(event.payload)
        })
      }
    }, 500)
  } catch (error) {
    console.log('activeSocket -> error', error)
    this.activesocket(true)
  }
}
