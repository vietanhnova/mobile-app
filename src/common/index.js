import axios from 'axios'
import { AsyncStorage } from 'react-native'
import QueryString from 'query-string'
const POST = 'post'
const PUT = 'put'
const DELETE = 'delete'
const GET = 'get'

export default class BaseAPI {
  static async getData (type) {
    return this.postGateWay(GET, type)
  }

  static async getDataWebsite (type) {
    return this.postGateWayWebsite(GET, type)
  }

  static async getDataAll (type) {
    return this.postGateWay(GET, type + '/getAll/')
  }

  static async postCreateData (body, type) {
    return this.postGateWay(POST, type, body)
  }

  static async putUpdateData (body, type) {
    return this.postGateWay(PUT, type, body)
  }

  static async putUpdateDataWebsite (body, type) {
    return this.postGateWayWebsite(PUT, type, body)
  }

  static async deleteData (body, type) {
    return this.postGateWay(DELETE, type + '?' + QueryString.stringify(body))
  }

  static async getConfig () {
    return this.postGateWay(GET, 'config/')
  }

  static async updateConfig (body) {
    return this.postGateWay(PUT, 'config', body)
  }

  static setItemStorage (key, value) {
    try {
      AsyncStorage.setItem(key, value)
    } catch (error) {
      // Error saving data
    }
  }

  static getItemStorage (key) {
    try {
      return AsyncStorage.getItem(key, (err, result) => { return !err ? result : false })
      // if (value !== null) {
      //     return JSON.parse(value);
      // }
    } catch (error) {
      // Error retrieving data
    }
  }

  static async postGateWay (method, action, body, linkServer) {
    try {
      let serverUrl = 'http://ush-server.ddns.net/api'
      const token = await BaseAPI.getItemStorage('auth')

      const config = {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      }
      let axiosInstance = axios.create(config)

      const response = await axiosInstance[method](serverUrl + action, body, config)
      if (response.status === 200) {
        return response.data
      } else {
        return null
      }
    } catch (error) {
      return null
    }
  }
}
